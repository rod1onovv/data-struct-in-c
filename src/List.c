#include "../include/List.h"

#include <stdio.h>
#include <stdlib.h>

struct Node {
    Data data;
    Node *next;
};

Node *initNode(Data data) {
    Node *list = (Node*) malloc(data * sizeof (Node));
    list->data = data;
    list->next = NULL;
    return list;
}

Node *addNode(Node *node, Data data) {
    return node;
}

Node *deleteNode(Node *node, Data data) {
    return node;
}

void *removeNode(Node *node) {

}
